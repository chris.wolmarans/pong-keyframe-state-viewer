import React, { Component } from "react";
import PropTypes from "prop-types";
import style from "../components/TabButton.module.scss";
import buttonStyle from "../assets/styles/button.module.scss";

class TabButton extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    activeTab: PropTypes.string.isRequired
  };

  onClick = () => {
    const { label, onClick } = this.props;
    onClick(label);
  };

  render() {
    const {
      onClick,
      props: { activeTab, label }
    } = this;

    let className = style["tab"];
    if (label === activeTab) {
      className += " " + style["tab--active"];
    }

    return (
      <li className={className}>
        <button
          type="button"
          onClick={onClick}
          className={buttonStyle["button"]}
        >
          {label}
        </button>
      </li>
    );
  }
}

export default TabButton;
