import React, { Component } from "react";
import PropTypes from "prop-types";
import TabButton from "./TabButton";
import style from "../components/Keyframe.module.scss";

class Keyframe extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Array).isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      activeTab: this.props.children[0].props.label
    };
  }

  onClickTab = tab => {
    this.setState({ activeTab: tab });
  };

  render() {
    const {
      onClickTab,
      props: { children },
      state: { activeTab }
    } = this;

    return (
      <div className={style['container']}>
        <ol className={style['tab-buttons']}>
          {children.map(child => {
            const { label } = child.props;
            return (
              <TabButton
                label={label}
                onClick={onClickTab}
                activeTab={activeTab}
                key={label}
              />
            );
          })}
        </ol>
        {children.map(child => {
          if (child.props.label !== activeTab) return undefined;
          return child;
        })}
      </div>
    );
  }
}

export default Keyframe;
