import React, { Component } from "react";
import PropTypes from "prop-types";
import style from "./View.module.scss";

class View extends Component {
  static propTypes = {
    label: PropTypes.string.isRequired
  };

  render() {
    const {
      props: { label }
    } = this;

    let className = style['container'];
    if (label === "three") {
      className += " " + style['three--active'];
    } else if (label === "two") {
      className += " " + style['two--active'];
    } else {
      className += " " + style['one--active'];
    }

    return (
      <div className={className}>
        <div className={style['paddle--left']} />
        <div className={style['ball-zone']}>
          <div className={style['ball']} />
        </div>
        <div className={style['paddle--right']} />
      </div>
    );
  }
}

export default View;
