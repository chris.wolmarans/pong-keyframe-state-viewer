import React from "react";
import style from "./App.module.scss";
import Keyframe from "./Keyframe";
import View from "./View";

function App() {
  return (
    <main className={style["container"]}>
      <div className={style["stage"]}>
        <Keyframe>
          <View label="one" />
          <View label="two" />
          <View label="three" />
        </Keyframe>
      </div>
    </main>
  );
}

export default App;
