## Pong Keyframe State Viewer

Clone the repo as a local project.

Run either `npm install` or `yarn` to install the dependencies.

Run `npm run start` or `yarn start` to start the server.


### Frontend designer skills exercise - estimated time: 2 hours

You should also have a file called ‘wireframes.png’ with this file.  Use it as a reference

![Alt text](./src/assets/images/wireframes.png?raw=true "Wireframe")

This exercise is inspired on the early video game pong (https://en.wikipedia.org/wiki/Pong)

There are 3 tab buttons and 3 views representing a different keyframe captured from the game

## Clicking a tab shows the corresponding view:
```
 tab ‘one’ => View 1
 tab ‘two’ => View 2
 tab ‘three’ => View 3
```

Use whatever markup structure, CSS rules and layout techniques you think appropriate to generate the views as specified in the wireframes. (wireframes.png)

## Palette:
```
Red: #EA0A0A
Blue: #0412AD
Grey: #979797
font-family: Helvetica sans-serif;
font-size: 16px;
```
You can use whatever frontend framework you are most comfortable with as this is as much about html/CSS as it is about JS but there is a bias towards React based solutions.


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
